import datetime

from sqlalchemy import Column, Integer, String, Text, DateTime, Table
from sqlalchemy.orm import mapper

from elo import mean_elo
from db import Base, metadata


class ScoredPhrase(Base):
    __tablename__ ='scoredStrings'
    id = Column(Integer, primary_key=True)
    base = Column(String, unique=False, nullable=False)
    translated = Column(Text)
    datetime_added = Column(DateTime)
    last_altered = Column(DateTime, default=datetime.datetime.utcnow)
    elo = Column(Integer, default=mean_elo)

    def __init__(self, base=None, text=None):
        self.base = base
        self.text = text

    def __repr__(self):
        return '<ScoredPhrase %r>' % self.base


strings = Table('scoredStrings', metadata,
                Column('id', Integer, primary_key=True),
                Column('base', String(50)),
                Column('translated', String(120)),
                Column('datetime_added', DateTime),
                Column('last_altered', DateTime),
                Column('elo', Integer)
                )
mapper(ScoredPhrase, strings, non_primary=True)
