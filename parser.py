import os, datetime

from handlers import *
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = os.path.join('uploads')
ALLOWED_EXTENSIONS = ['txt', 'xls', 'xlsx', 'json']
FILE_READERS = {
    'json': json.load,
    'xls': open_xls_file,
    'xlsx': open_xls_file,
    'txt': open
}

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


class Parser:
    def __init__(self, file, langs=('en','de','fr','it','es')):
        self.open_xlsx = open_xls_file
        self.save_json = save_to_json
        self.save_csv = save_to_csv
        self.save_pdf = save_to_pdf
        self.langs = langs
        self.file = file
        self.known = os.path.join('files/checked/{}/{}'.format(datetime.datetime.now(), self.file.filename))

    def start(self, directory=UPLOAD_FOLDER):
        data = self.open_xlsx(directory, self.file)
        json_file = self.save_json(self.file)
        return json_file

    def check_values(self):
        filename = secure_filename(self.file.filename)
        file_ext = filename.rsplit('.', 1)[1].lower()

        file_content = ''

        if allowed_file(filename) and file_ext in FILE_READERS:
            file_content = FILE_READERS[file_ext](os.path.join('files/uploads'), filename)

        known_json = json.loads(self.known)
        for lang in self.langs:
            check_file = os.path.join('files/check/{}'.format(lang))
            for key, value in file_content:
                if value in check_file:
                    known_json.dumps({
                        key: check_file[key]
                    })

        with open(known_json) as kj:
            kj.write(known_json)
